# DEPRECATION NOTICE
See https://gitlab.com/johnroberts/ansiblerole-firefox-general-browsing

This role is refactored/redesigned for infra-as-code: Firefox's `policies.json` and Betterfox-based `user.js` 

# Summary
This role installs and configures Firefox.

Features:
- Uses Firefox ESR directly from Mozilla (rather than Ubuntu's default Snap package)
- Supports up to three custom Firefox profiles (to have customized extensions, bookmarks, settings, etc.)

# Platforms
Tested against:
- Kubuntu 22.04
- Mint 21.1 (WIP)

May work (possibly with modifications) on other Debian or Ubuntu variants; adapting to other operating systems should be possible with a bit of tweaking.

# Usage
Read [defaults/main.yml](defaults/main.yml). Override vars as needed when calling the role.

Make sure the ansible host you run this from has the `community.general` collection available (install roles/collections with the `ansible-galaxy` command).

If you want to use custom profiles, read the next section.

# Custom Profiles
This role supports up to three custom Firefox profiles (`files/custom-profile-*`). Using custom profiles lets you have preferred extensions/themes, settings, bookmarks, etc.

## Default Custom Profiles
- (files/custom-profile-1)[files/custom-profile-1] is a general-purpose browsing profile
- (files/custom-profile-1)[files/custom-profile-2] is a security testing browsing profile
- (files/custom-profile-3)[files/custom-profile-3] is empty

## Adding Your Own Custom Profile
To use custom profiles, delete existing contents of `files/custom-profile-*`, and copy/paste in the profile data from an existing Firefox profile that you've configured to suit your preferences.
- Check the [Firefox docs on profile location](https://support.mozilla.org/en-US/kb/profiles-where-firefox-stores-user-data) to find the source directory to copy from
- You may wish to [clear personal data](https://support.mozilla.org/en-US/kb/delete-browsing-search-download-history-firefox) before copying your profile data in (especially if you'll be making a fork of this role public)

When copy/pasting into custom profile slots 1-3, delete all existing contents of those dirs while keeping the directory names the same (this role relies on these names).

`custom-profile-1` is the default profile when Firefox is launched.

# Firefox Package Selection (snap vs apt)
Modern versions of Ubuntu default to a Snap-based Firefox package. This presents some disadvantages relative to using an `apt` package:
- Slower disk access (and launch time)
- Non-standard configuration location (not using `~/.mozilla/firefox/`)
- Non-standard update mechanism (the process and config for updating Snap vs. apt packages differs)

Firefox comes in a few different versions/editions (like stable, development, and ESR). This role is aimed at balancing stability with security, so it chooses ESR (extended support release; aimed at enterprises).

For these reasons, this role uses Firefox-ESR installed from Mozilla-maintained PPA. By default this role will uninstall the default Firefox Snap package, add `ppa:mozillateam/ppa` to apt sources, and install the `firefox-esr` package. Note that this changes the default configuration directory to `~/.mozilla/firefox-esr`

# Future improvements
- Refactor to use:
    - Betterfox: https://github.com/yokoffing/Betterfox
    - Extensions installed via `policies.json` -> ExtensionSettings: https://mozilla.github.io/policy-templates/#extensionsettings
    - `policies.json`: https://support.mozilla.org/en-US/kb/customizing-firefox-using-policiesjson
    - Extension via  https://mozilla.github.io/policy-templates/#extensionsettings